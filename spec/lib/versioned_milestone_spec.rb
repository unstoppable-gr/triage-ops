require 'spec_helper'
require 'gitlab/triage/engine'
require 'versioned_milestone'

RSpec.describe VersionedMilestone do
  subject { described_class.new(context) }

  let(:context) do
    Gitlab::Triage::Resource::Context.build(resource, redact_confidentials: false)
  end

  let(:resource) { { type: 'issue', milestone: milestone_resource } }
  let(:milestone_resource) {}

  let(:all_non_expired) do
    [
      { title: '12.0' },
      { title: '12.1' },
      { title: '12.2' },
      { title: '12.3' },
      { title: 'Backlog' },
      { title: 'FY21' }
    ].map(&Gitlab::Triage::Resource::Milestone.method(:new))
  end

  before do
    allow(subject).to receive(:all_non_expired).and_return(all_non_expired)
  end

  describe '#all' do
    it 'does not return Backlog nor FY21' do
      all_titles = subject.all.map(&:title)

      expect(all_titles).not_to include('Backlog')
      expect(all_titles).not_to include('FY21')
    end
  end

  describe '#current' do
    it 'returns the first from all' do
      expect(subject.current.title).to eq('12.0')
    end
  end
end
